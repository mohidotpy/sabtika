# from django.shortcuts import render
# from django.views.generic import ListView, UpdateView
# from withdrawal.models import Withdrawal
# from django.utils.decorators import method_decorator
# from django.contrib.auth.decorators import login_required
# from publics.decorators import admin_required
#
#
# # Create your views here.
#
#
# class WithdrawalRequestList(ListView):
#     model = Withdrawal
#     template_name = 'admin/withdrawal/list.html'
#
#     @method_decorator(login_required)
#     @method_decorator(admin_required)
#     def dispatch(self, *args, **kwargs):
#         return super().dispatch(*args, **kwargs)
#
#     def get_queryset(self):
#         return Withdrawal.objects.filter(status='requested')
#
#
# class WithdrawalRequestApprove(UpdateView):
#     model = Withdrawal
#     template_name = 'admin/withdrawal/list.html'