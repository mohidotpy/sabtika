from django.views.generic import DetailView, ListView, CreateView
from .models import CompanyAdvertiseInvoice, CompanyLadderAdvertiseInvoice, CompanyUrgentAdvertiseInvoice
from django.contrib import messages
from django.utils.translation import gettext as _
from django.shortcuts import redirect
from .forms import CreateCAInvoiceForm, CreateCALadderInvoiceForm, CreateCAUrgentInvoiceForm
from django.http import Http404
from django.db.models import Q
from django.db import IntegrityError
from .constants import DEFAULT_AMOUNT, LADDER_AMOUNT, URGENT_AMOUNT
from .constants import INVOICE_TYPES


# Create your views here.
def CreateCAInvoice(request):
    try:
        if request.method == 'POST':
            if 'type' in request.POST:
                invoice_type = request.POST['type']
            else:
                invoice_type = INVOICE_TYPES[0][0]

            if invoice_type == INVOICE_TYPES[0][0]:
                form = CreateCAInvoiceForm(request.POST)
            elif invoice_type == INVOICE_TYPES[1][0]:
                form = CreateCALadderInvoiceForm(request.POST)
            elif invoice_type == INVOICE_TYPES[2][0]:
                form = CreateCAUrgentInvoiceForm(request.POST)
            else:
                raise ValueError({'detail': 'invoice type is not valid'})

            amount = DEFAULT_AMOUNT
            if invoice_type == INVOICE_TYPES[1][0]:
                amount = LADDER_AMOUNT
            elif invoice_type == INVOICE_TYPES[2][0]:
                amount = URGENT_AMOUNT

            if form.is_valid():
                invoice = form.save(commit=False)
                invoice.user = request.user
                invoice.amount = amount
                invoice.save()
                return redirect('ca_invoice:get', type=invoice_type, pk=invoice.id)
            else:
                for detail, error in form.errors.items():
                    messages.add_message(request, messages.ERROR, error)
                return redirect('advertises:list')

    except IntegrityError:
        upgrade_invoice = CompanyAdvertiseInvoice.objects.get(upgraded_user=request.user)
        messages.add_message(request, messages.WARNING, _('you have an invoice for this ad before.'))
        return redirect('invoice:get', pk=upgrade_invoice.id)
    except ValueError:
        raise Http404


#
# class GetLadderInvoice(DetailView):
#     template_name = 'company_advertise_invoice/detail.html'
#     context_object_name = 'invoice'
#     queryset = CompanyLadderAdvertiseInvoice.objects.all()
#
#
# class GetUrgentInvoice(DetailView):
#     model = CompanyUrgentAdvertiseInvoice
#     template_name = 'company_advertise_invoice/detail.html'
#     context_object_name = 'invoice'
#     queryset = CompanyUrgentAdvertiseInvoice.objects.all()


class GetInvoice(DetailView):
    template_name = 'company_advertise_invoice/detail.html'
    context_object_name = 'invoice'
    queryset = CompanyAdvertiseInvoice.objects.all()

    def get_queryset(self):
        if self.kwargs['type'] == INVOICE_TYPES[0][0]:
            return CompanyAdvertiseInvoice.objects.all()
        elif self.kwargs['type'] == INVOICE_TYPES[1][0]:
            return CompanyLadderAdvertiseInvoice.objects.all()
        elif self.kwargs['type'] == INVOICE_TYPES[2][0]:
            return CompanyUrgentAdvertiseInvoice.objects.all()
        else:
            raise Http404

    def get(self, request, *args, **kwargs):
        if 'payment' in request.GET:
            invoice = self.get_object()
            invoice.payment = True
            invoice.tracking_code = 456478
            invoice.save()
            messages.add_message(request, messages.SUCCESS, _('your payment successfully done!'))
            return redirect('invoice:history')

        return super(GetInvoice, self).get(request, *args, **kwargs)

    def get_object(self, queryset=None):
        object = super(GetInvoice, self).get_object(queryset=None)
        if object.company_advertise.user == self.request.user:
            return object
        else:
            raise Http404


class InvoiceHistory(ListView):
    # model = CompanyAdvertiseInvoice
    template_name = 'company_advertise_invoice/history.html'

    def get_queryset(self):
        query ='''SELECT * from advertises_advertises AS t1
JOIN
(SELECT *,'regular' as type FROM company_advertise_invoice_companyadvertiseinvoice
UNION
SELECT *,'ladder' as type FROM company_advertise_invoice_companyladderadvertiseinvoice
UNION
    SELECT *,'urgent' as type FROM company_advertise_invoice_companyurgentadvertiseinvoice
ORDER BY creation_date DESC) AS t2
on t1.id = t2.company_advertise_id
WHERE  t1.user_id=''' + str(self.request.user.id)
        return CompanyAdvertiseInvoice.objects.raw(query)
