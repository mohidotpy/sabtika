from django.urls import path, re_path
from . import views

app_name = 'ca_invoice'
urlpatterns = [
    path('create/', views.CreateCAInvoice, name='create'),
    path('history/', views.InvoiceHistory.as_view(), name='history'),
    path('<str:type>/<int:pk>/', views.GetInvoice.as_view(), name='get'),
]
