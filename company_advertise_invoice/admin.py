from django.contrib import admin
from .models import CompanyAdvertiseInvoice


class CAInvoice(admin.ModelAdmin):
    list_display = ('user',)

    def user(self, obj):
        return obj.company_advertise.user


admin.site.register(CompanyAdvertiseInvoice, CAInvoice)
