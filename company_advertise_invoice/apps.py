from django.apps import AppConfig


class CompanyAdvertiseInvoiceConfig(AppConfig):
    name = 'company_advertise_invoice'


    def ready(self):
        import company_advertise_invoice.signals
