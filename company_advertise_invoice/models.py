from django.db import models
from invoice.models import Invoice
from advertises.models import Advertises
from .constants import INVOICE_TYPES


# Create your models here.
class CompanyAdvertiseInvoice(Invoice):
    company_advertise = models.OneToOneField(Advertises, on_delete=models.CASCADE)

class CompanyLadderAdvertiseInvoice(Invoice):
    company_advertise = models.ForeignKey(Advertises, on_delete=models.CASCADE)

class CompanyUrgentAdvertiseInvoice(Invoice):
    company_advertise = models.OneToOneField(Advertises, on_delete=models.CASCADE)

    # @staticmethod
    # def create_invoice_if_not_exist(test_id):
    #     test = UserTests.objects.get(id=test_id)
    #     if not hasattr(test, 'invoice'):
    #         kwargs = {'test': test, 'amount': test.calculate_amount_for_invoice(), 'payment': False, 'type': 1}
    #         return Invoice.objects.create(**kwargs)
    #     return test.invoice
    #
    # def update_invoice(self):
    #     test = self.test
    #     amount = test.calculate_amount_for_invoice()
    #     self.amount = amount
    #     self.save()
