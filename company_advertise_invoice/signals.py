from .models import CompanyAdvertiseInvoice
from django.db.models.signals import post_save, post_delete
from advertises.models import Advertises
from .constants import DEFAULT_AMOUNT


def create_ca_invoice(sender, instance, **kwargs):
    if kwargs['created'] == True:
        ca_invoice = CompanyAdvertiseInvoice(company_advertise=instance, amount=DEFAULT_AMOUNT)
        ca_invoice.save()


post_save.connect(create_ca_invoice, sender=Advertises)

#
# def update_invoice(sender, instance, **kwargs):
#     if kwargs['update_fields'] != None:
#         invoice = instance.invoice
#         if 'questions_number' in kwargs['update_fields'] or 'users_number' in kwargs['update_fields']:
#             invoice.update_invoice()
#
# post_save.connect(update_invoice, sender=UserTests)
