from django.forms import ModelForm
from .models import CompanyAdvertiseInvoice, CompanyLadderAdvertiseInvoice, CompanyUrgentAdvertiseInvoice


class CreateCAInvoiceForm(ModelForm):
    class Meta:
        model = CompanyAdvertiseInvoice
        fields = ['company_advertise']


class CreateCALadderInvoiceForm(ModelForm):
    class Meta:
        model = CompanyLadderAdvertiseInvoice
        fields = ['company_advertise']


class CreateCAUrgentInvoiceForm(ModelForm):
    class Meta:
        model = CompanyUrgentAdvertiseInvoice
        fields = ['company_advertise']
