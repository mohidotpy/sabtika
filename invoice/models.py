from users.models import Users
from django.db import models
import uuid


class Invoice(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    amount = models.DecimalField(max_digits=9, decimal_places=0)
    payment = models.BooleanField(default=False)
    tracking_code = models.BigIntegerField(null=True)
    unique_code = models.UUIDField(default=uuid.uuid4)

    class Meta:
        abstract = True
