from django.contrib import admin
from .models import Withdrawal


class WithdrawalAdmin(admin.ModelAdmin):
    list_display = ('user', 'status')


admin.site.register(Withdrawal, WithdrawalAdmin)
