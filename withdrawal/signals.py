from django.db.models.signals import post_save
from withdrawal.models import Withdrawal
from users.tasks import send_email
from company_advertise_invoice.models import CompanyLadderAdvertiseInvoice,CompanyUrgentAdvertiseInvoice

def email_approvement_withdrawal_email(sender, instance, **kwargs):
    if kwargs['update_fields']:
        if 'status' in kwargs['update_fields'] and instance.status == 'deposited_to_user':
            user = instance.user
            message = '{} تومان به درخواست شما به حساب {} واریز شد.'.format(instance.amount, user.account_number)
            send_email.delay(subject='واریز پول', message=message, receptions=[user.email])


post_save.connect(email_approvement_withdrawal_email, sender=Withdrawal)

