from django.forms import ModelForm
from .models import Withdrawal


class WithdrawalRequestForm(ModelForm):
    class Meta:
        model = Withdrawal
        fields = ['user', 'amount']
