from django.db import models
from users.models import Users
from django.utils.translation import gettext as _


# Create your models here.


class Withdrawal(models.Model):
    status = (
        ('requested', _('requested')),
        ('deposited_to_user', _('deposited_to_user')),
        ('problem', _('problem'))
    )

    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=True)
    status = models.CharField(choices=status, max_length=20, default=status[0][0])
    amount = models.DecimalField(decimal_places=0, max_digits=9)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        db_wt = Withdrawal.objects.filter(pk=self.pk).first()
        if db_wt != None:
            if db_wt.status != self.status:
                update_fields = ['status']
            return super(Withdrawal, self).save(force_insert=False, force_update=False, using=None,
                                                update_fields=update_fields)
        return super(Withdrawal, self).save(force_insert=False, force_update=False, using=None,
                                            update_fields=None)
