from django.urls import path
from . import views

app_name = 'withdrawal'
urlpatterns = [
    path('create/', views.withdrawal_request, name='create'),
    path('list/', views.WithdrawalList.as_view(), name='list'),
]
