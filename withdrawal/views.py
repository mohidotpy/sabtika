from django.shortcuts import redirect
from publics.decorators import bank_info_required
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .forms import WithdrawalRequestForm
from django.utils.translation import gettext as _
from django.views.generic import ListView
from .models import Withdrawal
from django.utils.decorators import method_decorator
from publics.decorators import bank_info_required, email_confirmed_required


# Create your views here.

@login_required
@bank_info_required
def withdrawal_request(request):
    if request.method == 'POST':
        form = WithdrawalRequestForm(request.POST)
        if form.is_valid():
            form.cleaned_data['status'] = 'requested'
            form.save()
            messages.add_message(request, messages.SUCCESS, _('your withdrawal request received successfully.'))
            return redirect('users:dashboard')

        messages.add_message(request, messages.ERROR, _('there is a problem, try again or contact us.'))
        return redirect('users:dashboard')

    else:
        messages.add_message(request, messages.SUCCESS, _('access directly restricted.'))
        return redirect('users:dashboard')


class WithdrawalList(ListView):
    model = Withdrawal
    template_name = 'withdrawal/list.html'

    @method_decorator(login_required)
    @method_decorator(email_confirmed_required)
    @method_decorator(bank_info_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):
        return Withdrawal.objects.filter(user=self.request.user)
