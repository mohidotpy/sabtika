from django.apps import AppConfig


class WithdrawalConfig(AppConfig):
    name = 'withdrawal'

    def ready(self):
        import withdrawal.signals
