$(document).ready(function () {

    $("#id_has_grade").change(function () {
        var status = $(this).prop('checked');
        if (status == true) {
            $('#show-grade').css('visibility', 'visible');
            $('#id_grade').removeAttr('disabled');

        } else {

            $('#show-grade').css('visibility', 'hidden');
            $('#id_grade').attr('disabled', 'disabled');

        }

    });


});
