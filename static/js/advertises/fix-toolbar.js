$(document).ready(function () {
    $(document).scroll(function () {
        if ($(document).scrollTop() > 168 && $(document).scrollTop() <= 906) {
            $('#wrr').addClass("fix-menu");
        } else {
            $('#wrr').removeClass("fix-menu");
        }

        if ($(document).scrollTop() > 906) {
            $('#wrr').addClass("fix-bottom-menu");
        } else {
            $('#wrr').removeClass("fix-bottom-menu");
        }
    });
});
