
document.getElementById('profile_picture').addEventListener('change', readURL, true);
function readURL(){
    var file = document.getElementById("profile_picture").files[0];
    var reader = new FileReader();
    reader.onloadend = function(){
        document.getElementById('profile-image').style.backgroundImage = "url(" + reader.result + ")";
    }
    if(file){
        reader.readAsDataURL(file);
    }else{
    }
}
