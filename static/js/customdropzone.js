
document.getElementById('first_pic').addEventListener('change', readURL, true);
function readURL(){
    var file = document.getElementById("first_pic").files[0];
    var reader = new FileReader();
    reader.onloadend = function(){
        document.getElementById('profile-upload').style.backgroundImage = "url(" + reader.result + ")";
    }
    if(file){
        reader.readAsDataURL(file);
    }else{
    }
}
