# from .models import Users
# from wallet.models import Wallet
# from django.db.models.signals import post_save, post_delete
# from inner_transaction.models import InnerTransactions
# from withdrawal.models import Withdrawal
# from django.db import transaction as db_transaction
#
#
# def create_user_wallet(sender, instance, **kwargs):
#     try:
#         instance.wallet
#     except Wallet.DoesNotExist:
#         wallet = Wallet.objects.create(user=instance, amount=0.00)
#
#
# post_save.connect(create_user_wallet, sender=Users)
#
#
# def add_to_wallet(sender, instance, **kwargs):
#     wallet = instance.selected_test.participator.wallet
#     wallet.amount = wallet.amount + instance.amount
#     wallet.save(update_fields=['amount', ])
#
#
# post_save.connect(add_to_wallet, sender=InnerTransactions)
#
#
# def withdrawal_from_wallet_and_add_to_requested(sender, instance, **kwargs):
#     wallet = instance.user.wallet
#     amount = instance.amount
#     with db_transaction.atomic():
#         wallet.amount = wallet.amount - amount
#         wallet.withdrawal_pending_amount = wallet.withdrawal_pending_amount + amount
#         wallet.save()
#
#
# post_save.connect(withdrawal_from_wallet_and_add_to_requested, sender=Withdrawal)
