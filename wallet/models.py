from django.db import models
from users.models import Users


# Create your models here.


class Wallet(models.Model):
    user = models.OneToOneField(Users, on_delete=models.CASCADE)
    amount = models.DecimalField(decimal_places=0, max_digits=12)
    withdrawal_pending_amount = models.DecimalField(decimal_places=0, max_digits=12, default=0)
