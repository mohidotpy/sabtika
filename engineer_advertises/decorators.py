from functools import wraps
from django.shortcuts import redirect
from django.contrib import messages
from django.utils.translation import gettext as _
from .models import EngineerAdvertises

def owner_required(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        try:
            user_test = EngineerAdvertises.objects.get(pk=kwargs['pk'])
            if request.user == user_test.user:
                return function(request, *args, **kwargs)
            messages.add_message(request, messages.ERROR, _("you can't access to this page."))
            return redirect('users:dashboard')
        except EngineerAdvertises.DoesNotExist:
            messages.add_message(request, messages.ERROR, _("you can't access to this page."))
            return redirect('users:dashboard')
    return wrap
