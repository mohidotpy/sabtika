from django.db import models
from users.models import Users
from django.core.validators import MaxValueValidator, MinValueValidator
from django.urls import reverse
from .constants import CITIES, UNI_DEGREE, ADVERTISE_TYPE


# Create your models here.
class EngineerAdvertises(models.Model):
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    field = models.CharField(max_length=100, null=True)
    description = models.CharField(max_length=1000, null=True)
    degree = models.CharField(choices=UNI_DEGREE, max_length=30, blank=False, null=False)
    insurance_history = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(800)], null=True,
                                            blank=True)
    # location = models.CharField(choices=CITIES, max_length=30)
    # has_grade = models.BooleanField()
    price = models.DecimalField(decimal_places=0, max_digits=12)
    creation_date = models.DateTimeField(auto_now_add=True)
    is_approved = models.BooleanField(default=False)
    email_contact = models.BooleanField(default=False)
    mobile_contact = models.BooleanField(default=False)
    ladder_date = models.DateTimeField()
    urgent = models.BooleanField(default=False)
    advertise_type = models.CharField(choices=ADVERTISE_TYPE, max_length=30)

    def get_absolute_url(self):
        if self.engineeradvertiseinvoice.payment:
            return (reverse('engineer_advertises:my_list'))
        else:
            return (reverse('en_invoice:get', kwargs={'pk': self.engineeradvertiseinvoice.id, 'type': 'regular'}))
