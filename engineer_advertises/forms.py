from django import forms
from django.forms import ModelForm
from .models import EngineerAdvertises
from django.utils.translation import gettext as _
from .constants import UNI_DEGREE


class UpdateEngineerAdvertiseForm(ModelForm):
    has_insurance_history = forms.BooleanField()

    def __init__(self, *args, **kwargs):
        super(UpdateEngineerAdvertiseForm, self).__init__(*args, **kwargs)

        self.fields['description'].widget.attrs = {'class': 'materialize-textarea'}
        self.fields['has_insurance_history'].required = False

        if kwargs['instance'].insurance_history == None:
            self.fields['insurance_history'].widget.attrs = {"min": "1", "max": "800", 'disabled': 'disabled'}
        else:
            self.fields['insurance_history'].widget.attrs = {"min": "1", "max": "800"}

        if kwargs['instance'].insurance_history != None:
            self.fields['has_insurance_history'].widget.attrs = {"checked": "checked"}

    class Meta:
        model = EngineerAdvertises
        fields = ['field', 'degree', 'insurance_history', 'price', 'description',
                  'price']
        widgets = {
            'description': forms.Textarea,
        }


class CreateEngineerAdvertiseForm(ModelForm):
    has_insurance_history = forms.BooleanField()
    accuracy = forms.BooleanField()

    def __init__(self, *args, **kwargs):
        super(CreateEngineerAdvertiseForm, self).__init__(*args, **kwargs)

        self.fields['description'].widget.attrs = {'class': 'materialize-textarea'}
        self.fields['has_insurance_history'].required = False
        self.fields['insurance_history'].widget.attrs = {"min": "1", "max": "800", 'disabled': 'disabled'}

        # if kwargs['instance'].insurance_history == None:
        #     self.fields['insurance_history'].widget.attrs = {"min": "1", "max": "800", 'disabled': 'disabled'}
        # else:
        #     self.fields['insurance_history'].widget.attrs = {"min": "1", "max": "800"}
        #
        # if kwargs['instance'].insurance_history != None:
        #     self.fields['has_insurance_history'].widget.attrs = {"checked": "checked"}

    class Meta:
        model = EngineerAdvertises
        fields = ['field', 'degree', 'insurance_history', 'price', 'description',
                  'price', 'advertise_type']
        widgets = {
            'description': forms.Textarea,
        }
