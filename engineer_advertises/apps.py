from django.apps import AppConfig


class EngineerAdvertisesConfig(AppConfig):
    name = 'engineer_advertises'

    def ready(self):
        import engineer_advertises.signals
