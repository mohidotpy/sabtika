from django.views.generic import ListView, CreateView, UpdateView, DetailView
from .models import EngineerAdvertises
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from .forms import CreateEngineerAdvertiseForm, UpdateEngineerAdvertiseForm
from publics.decorators import mobile_confirmed_required
from .decorators import owner_required
from django.shortcuts import render
from faker import Faker
from django.http import HttpResponse
from .constants import UNI_DEGREE, ADVERTISE_TYPE
from django.utils import timezone
from django.contrib import messages
from django.shortcuts import redirect

# Create your views here.

class EngineerAdvertisesList(ListView):
    # page_template = 'advertises/list_page.html'
    template_name = 'engineer_advertises/list.html'
    paginate_by = 10

    # @method_decorator(login_required)
    # @method_decorator(email_confirmed_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):

        filters = {}
        if 'field' in self.request.GET:
            filters['field__icontains'] = self.request.GET['field']

        if 'degree' in self.request.GET:
            filters['degree'] = self.request.GET['degree']

        if 'insurance-from' in self.request.GET:
            filters['insurance_history__gte'] = self.request.GET['insurance-from']

        if 'insurance-to' in self.request.GET:
            filters['insurance_history__lte'] = self.request.GET['insurance-to']

        if 'price-from' in self.request.GET:
            filters['price__gte'] = self.request.GET['price-from']

        if 'price-to' in self.request.GET:
            filters['price__lte'] = self.request.GET['price-to']

        if 'advertise-type' in self.request.GET:
            filters['advertise_type'] = self.request.GET['advertise-type']

        if filters == {}:
            return EngineerAdvertises.objects.filter(engineeradvertiseinvoice__payment__exact=True).order_by('-ladder_date')
        else:
            return EngineerAdvertises.objects.filter(**filters, engineeradvertiseinvoice__payment__exact=True).order_by(
                '-ladder_date')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(EngineerAdvertisesList, self).get_context_data(object_list=None, **kwargs)
        context['degrees'] = UNI_DEGREE
        context['advertise_type'] = ADVERTISE_TYPE
        # context['cities'] = CITIES

        for obj in context['object_list']:
            obj.price = format(int(obj.price), ",d")

        return context

    def get(self, request, *args, **kwargs):

        return super(EngineerAdvertisesList, self).get(request, *args, **kwargs)


class MyEngineerAdvertisesList(ListView):
    # page_template = 'advertises/list_page.html'
    template_name = 'engineer_advertises/my-list.html'
    paginate_by = 10

    # @method_decorator(login_required)
    # @method_decorator(email_confirmed_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):
        return EngineerAdvertises.objects.filter(user=self.request.user).order_by('-ladder_date')


class CreateEngineerAd(CreateView):
    form_class = CreateEngineerAdvertiseForm
    queryset = EngineerAdvertises.objects.all()
    template_name = 'engineer_advertises/create.html'

    @method_decorator(login_required)
    @method_decorator(mobile_confirmed_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        # if not request.user.first_name or not request.user.profile_picture:
        #     messages.add_message(self.request, messages.ERROR,
        #                          _('Your have to set your profile picture and name first! '))
        #     return redirect('users:profile', pk=request.user.id)

        return super(CreateEngineerAd, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        if form.cleaned_data['accuracy'] != True:
            messages.add_message(self.request, messages.ERROR, 'شما باید قوانین و مقررات را قبول کنید.')
            return redirect('advertises:create')
        form.instance.user = self.request.user

        now = timezone.now()
        form.instance.ladder_date = now
        return super(CreateEngineerAd, self).form_valid(form)


class Statistic(DetailView):
    model = EngineerAdvertises
    template_name = 'advertises/statistics.html'

    @method_decorator(login_required)
    @method_decorator(mobile_confirmed_required)
    @method_decorator(owner_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class EngineerAdvertiseDetail(DetailView):
    model = EngineerAdvertises
    template_name = 'engineer_advertises/detail.html'

    # @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EngineerAdvertiseDetail, self).get_context_data(**kwargs)
        context['object'].price = format(int(context['object'].price), ",d")
        return context


class UpdateEngineerAdvertise(UpdateView):
    form_class = UpdateEngineerAdvertiseForm
    queryset = EngineerAdvertises.objects.all()
    template_name = 'engineer_advertises/update.html'

    @method_decorator(login_required)
    @method_decorator(mobile_confirmed_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


def create_fake_ads(request):
    user = request.user
    fake = Faker()
    for i in range(int(request.GET['num'])):
        field = 'مهندسی عمران'
        description = 'اول تو واتس آپ'
        degree = 2
        insurance_history = 30
        price = 25
        ad = EngineerAdvertises.objects.create(user=user, field=field, description=description, degree=degree,
                                               insurance_history=insurance_history,
                                               price=price)

    return HttpResponse('ok')


def ajax_list(request):
    # numbers_list = range(1, 1000)
    # page = request.GET.get('page', 1)
    # paginator = Paginator(numbers_list, 20)
    # try:
    #     numbers = paginator.page(page)
    # except PageNotAnInteger:
    #     numbers = paginator.page(1)
    # except EmptyPage:
    #     numbers = paginator.page(paginator.num_pages)
    return render(request, 'advertises/list_page.html')
