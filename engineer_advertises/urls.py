from django.urls import path
from . import views

app_name = 'engineer_advertises'

urlpatterns = [
    path('list/', views.EngineerAdvertisesList.as_view(), name='list'),
    path('mine/', views.MyEngineerAdvertisesList.as_view(), name='my_list'),
    path('<int:pk>/', views.EngineerAdvertiseDetail.as_view(), name='detail'),
    path('create/', views.CreateEngineerAd.as_view(), name='create'),
    path('statistic/<int:pk>/', views.Statistic.as_view(), name='statistic'),
    path('update/<int:pk>/', views.UpdateEngineerAdvertise.as_view(), name='update'),
    path('fake/', views.create_fake_ads, name='fake'),

]
