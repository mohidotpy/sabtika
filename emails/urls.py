from django.urls import re_path, path
from . import views

app_name = 'emails'

urlpatterns = [
    # url(r'^account_activation_sent/$', core_views.account_activation_sent, name='account_activation_sent'),
    re_path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            views.activate, name='activate'),
    path('resend_email/', views.resend_email, name='resend_email')
]
