from django.contrib.auth import login
from django.shortcuts import render, redirect
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from .tokens import account_activation_token
from users.models import Users
from django.contrib.auth import login, logout
from django.contrib import messages
from django.utils.translation import gettext as _
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.decorators import login_required
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.core.mail import get_connection
from .constants import alt_host,alt_password,alt_port,alt_use_tls,alt_username, pri_host, pri_password, pri_port, pri_use_tls, pri_username

def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = Users.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, Users.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_verified = True
        user.save()
        if user != request.user:
            if request.user.is_anonymous:
                login(request, user)
            else:
                logout(request)
                login(request, user)
        messages.add_message(request, messages.SUCCESS, _('ایمیل شما با موفقیت تایید شد'))

        # send welcome email
        subject = 'خوش آمدید|ثبتیکا'
        current_site = get_current_site(request)
        message = render_to_string('emails/welcome.html', {
            'user': user,
            'domain': current_site.domain,
        })
        connection = get_connection(host=pri_host,
                                    port=pri_port,
                                    username=pri_username,
                                    password=pri_password,
                                    use_tls=pri_use_tls)
        email = EmailMultiAlternatives(subject, message, from_email='info@sabtika.ir', to=[user.email],
                                       connection=connection)
        email.send()

        return redirect('users:dashboard')
    else:
        messages.add_message(request, messages.ERROR, _('لینک ایمیل شما نامعتبر است. دوباره تلاش کنید.'))
        return redirect('users:dashboard')


@login_required
def resend_email(request):
    current_site = get_current_site(request)
    subject = 'تاییدیه ایمیل|ثبتیکا'
    user = request.user

    html_content = render_to_string('emails/confirm_email.html', {
        'user': user,
        'domain': current_site.domain,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode("utf-8"),
        'token': account_activation_token.make_token(user),
    })
    # reception is a dict even with one email
    connection = get_connection(host=alt_host,
                                port=alt_port,
                                username=alt_username,
                                password=alt_password,
                                use_tls=alt_use_tls)
    text_content = 'This is an important message.'
    email = EmailMultiAlternatives(subject, text_content,from_email='sabtika.ir@gmail.com', to=[user.email],connection=connection)
    email.attach_alternative(html_content, "text/html")

    email.send()
    messages.add_message(request, messages.SUCCESS, _('email sent again'))
    return redirect('users:dashboard')


def test(request):
    current_site = get_current_site(request)
    subject = 'تاییدیه ایمیل|ثبتیکا'
    user = request.user
    return render(request, 'emails/confirm_email.html', {
        'user': user,
        'domain': current_site.domain,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode("utf-8"),
        'token': account_activation_token.make_token(user),
    })
