from django import forms
from django.forms import ModelForm
from .models import Advertises
from django.utils.translation import gettext as _


class CreateAdvertiseForm(ModelForm):
    # questions_number = forms.IntegerField(widget=forms.NumberInput(attrs={'type':'range', 'step': '2'}))
    has_grade = forms.BooleanField()
    accuracy = forms.BooleanField()

    def __init__(self, *args, **kwargs):
        super(CreateAdvertiseForm, self).__init__(*args, **kwargs)
        self.fields['grade'].widget.attrs = {"min": "1", "max": "5", "step": "1",
                                             'class': 'users_number_input', 'disabled': 'disabled'}
        self.fields['register_year'].widget.attrs = {"min": "1358", "max": "1397", "step": "1",
                                                     'class': 'questions_number_input'}
        self.fields['description'].widget.attrs = {'class': 'materialize-textarea'}
        self.fields['has_grade'].required = False
        self.fields['financial_code'].required = False

    class Meta:
        model = Advertises
        fields = ['company_name', 'description', 'type', 'register_year', 'register_location', 'financial_code',
                  'price', 'grade', 'has_grade', 'company_pic', 'advertise_type']
        widgets = {
            'grade': forms.NumberInput(attrs={'type': 'range'}),
            'register_year': forms.NumberInput(attrs={'type': 'range'}),
            'description': forms.Textarea,
        }


class UpdateAdvertiseForm(ModelForm):
    # questions_number = forms.IntegerField(widget=forms.NumberInput(attrs={'type':'range', 'step': '2'}))
    has_grade = forms.BooleanField()

    def __init__(self, *args, **kwargs):
        super(UpdateAdvertiseForm, self).__init__(*args, **kwargs)
        self.fields['grade'].widget.attrs = {"min": "1", "max": "5", "step": "1",
                                             'class': 'users_number_input', 'disabled': 'disabled'}
        self.fields['register_year'].widget.attrs = {"min": "1358", "max": "1397", "step": "1",
                                                     'class': 'questions_number_input'}
        self.fields['description'].widget.attrs = {'class': 'materialize-textarea'}
        self.fields['has_grade'].required = False
        self.fields['financial_code'].required = False

    class Meta:
        model = Advertises
        fields = ['company_name', 'description', 'type', 'register_year', 'register_location', 'financial_code',
                  'price', 'grade', 'has_grade', 'company_pic', 'advertise_type']
        widgets = {
            'grade': forms.NumberInput(attrs={'type': 'range'}),
            'register_year': forms.NumberInput(attrs={'type': 'range'}),
            'description': forms.Textarea,
        }
