from django.db import models
from users.models import Users
from django.core.validators import MaxValueValidator, MinValueValidator
from django.urls import reverse
from .constants import CITIES, COMPANY_TYPES, ADVERTISE_TYPE


# Create your models here.
class Advertises(models.Model):
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    company_name = models.CharField(max_length=100, null=True)
    description = models.CharField(max_length=1000, null=True)
    type = models.CharField(choices=COMPANY_TYPES, max_length=30)
    register_year = models.IntegerField(validators=[MinValueValidator(1340), MaxValueValidator(1398)])
    register_location = models.CharField(choices=CITIES, max_length=30)
    financial_code = models.BooleanField()
    price = models.DecimalField(decimal_places=0, max_digits=12)
    creation_date = models.DateTimeField(auto_now_add=True)
    grade = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)], null=True, blank=True)
    company_pic = models.ImageField(null=True, blank=True)
    is_approved = models.BooleanField(default=False)
    email_contact = models.BooleanField(default=False)
    mobile_contact = models.BooleanField(default=False)
    ladder_date = models.DateTimeField()
    urgent = models.BooleanField(default=False)
    advertise_type = models.CharField(choices=ADVERTISE_TYPE, max_length=30)

    def get_absolute_url(self):
        if self.companyadvertiseinvoice.payment:
            return (reverse('advertises:my_list'))
        else:
            return (reverse('ca_invoice:get', kwargs={'pk': self.companyadvertiseinvoice.id, 'type': 'regular'}))
            #
            # @staticmethod
            # def is_owner(pk, user):
            #     # return Advertises.objects.filter(pk=pk).first().user == user
            #     user_test = Advertises.objects.get(pk=pk)
            #     return user_test.user == user
            #
            # def calculate_amount_for_invoice(self):
            #     return (self.users_number * self.per_user_price) + (self.questions_number * self.per_question_price)
            #
            # def save(self, force_insert=False, force_update=False, using=None,
            #          update_fields=None):
            #     db_us = Advertises.objects.filter(pk=self.pk).first()
            #
            #     if db_us != None:
            #         update_fields = []
            #         if db_us.questions_number != self.questions_number:
            #             update_fields = ['questions_number']
            #
            #         if db_us.users_number != self.users_number:
            #             update_fields.append('users_number')
            #
            #         if db_us.title != self.title:
            #             update_fields.append('title')
            #
            #         if update_fields == []:
            #             update_fields = None
            #
            #     return super(Advertises, self).save(force_insert=False, force_update=False, using=None,
            #                                        update_fields=update_fields)
