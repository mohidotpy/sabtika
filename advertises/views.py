from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView, DetailView
from .models import Advertises
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from .forms import CreateAdvertiseForm, UpdateAdvertiseForm
from publics.decorators import mobile_confirmed_required
from .decorators import owner_required
from django.shortcuts import render
from faker import Faker
from django.http import HttpResponse
from .constants import COMPANY_TYPES, CITIES, ADVERTISE_TYPE
from django.utils import timezone
from django.contrib import messages
from django.shortcuts import redirect


# Create your views here.

class AdvertisesList(ListView):
    # page_template = 'advertises/list_page.html'
    template_name = 'advertises/list.html'
    paginate_by = 10

    # @method_decorator(login_required)
    # @method_decorator(mobile_confirmed_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):

        filters = {}
        if 'financial-code-checkbox' in self.request.GET:
            if self.request.GET['financial-code-checkbox'] == 'on':
                filters['financial_code'] = True

        if 'company-type' in self.request.GET:
            filters['type'] = self.request.GET['company-type']

        if 'city' in self.request.GET:
            filters['city'] = self.request.GET['city']

        if 'year-from' in self.request.GET:
            filters['register_year__gte'] = self.request.GET['year-from']

        if 'year-to' in self.request.GET:
            filters['register_year__lte'] = self.request.GET['year-to']

        if 'grade-from' in self.request.GET:
            filters['grade__gte'] = self.request.GET['grade-from']

        if 'grade-to' in self.request.GET:
            filters['grade__lte'] = self.request.GET['grade-to']

        if 'price-from' in self.request.GET:
            filters['price__gte'] = self.request.GET['price-from']

        if 'price-to' in self.request.GET:
            filters['price__lte'] = self.request.GET['price-to']

        if 'advertise-type' in self.request.GET:
            filters['advertise_type'] = self.request.GET['advertise-type']

        if filters == {}:
            return Advertises.objects.filter(companyadvertiseinvoice__payment__exact=True).order_by('-ladder_date')
        else:
            return Advertises.objects.filter(**filters, companyadvertiseinvoice__payment__exact=True).order_by(
                '-ladder_date')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(AdvertisesList, self).get_context_data(object_list=None, **kwargs)
        # add first pic of first question to tests
        context['company_types'] = COMPANY_TYPES
        context['cities'] = CITIES
        context['advertise_type'] = ADVERTISE_TYPE

        for obj in context['object_list']:
            obj.price = format(int(obj.price), ",d")

        return context

    def get(self, request, *args, **kwargs):

        return super(AdvertisesList, self).get(request, *args, **kwargs)


class MyAdvertisesList(ListView):
    # page_template = 'advertises/list_page.html'
    template_name = 'advertises/my-list.html'
    paginate_by = 10

    @method_decorator(login_required)
    # @method_decorator(email_confirmed_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):
        return Advertises.objects.filter(user=self.request.user).order_by('-ladder_date')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(MyAdvertisesList, self).get_context_data(object_list=None, **kwargs)
        # add first pic of first question to tests
        context['company_types'] = COMPANY_TYPES
        context['cities'] = CITIES

        return context


class CreateAd(CreateView):
    form_class = CreateAdvertiseForm
    queryset = Advertises.objects.all()
    template_name = 'advertises/create.html'

    @method_decorator(login_required)
    @method_decorator(mobile_confirmed_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        # if not request.user.first_name or not request.user.profile_picture:
        #     messages.add_message(self.request, messages.ERROR,
        #                          _('Your have to set your profile picture and name first! '))
        #     return redirect('users:profile', pk=request.user.id)

        return super(CreateAd, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        if form.cleaned_data['accuracy'] != True:
            messages.add_message(self.request, messages.ERROR, 'شما باید قوانین و مقررات را قبول کنید.')
            return redirect('advertises:create')
        form.instance.user = self.request.user

        now = timezone.now()
        form.instance.ladder_date = now

        return super(CreateAd, self).form_valid(form)


class UpdateTest(UpdateView):
    form_class = CreateAdvertiseForm
    queryset = Advertises.objects.all()
    template_name = 'advertises/update_test.html'

    @method_decorator(login_required)
    @method_decorator(mobile_confirmed_required)
    @method_decorator(owner_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(UpdateTest, self).form_valid(form)


class Statistic(DetailView):
    model = Advertises
    template_name = 'advertises/statistics.html'

    @method_decorator(login_required)
    @method_decorator(mobile_confirmed_required)
    @method_decorator(owner_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


        # def get_context_data(self, **kwargs):
        # context = super(Statistic, self).get_context_data(**kwargs)
        # questions = Questions.objects.filter(test=kwargs['object'])
        # for q in questions:
        #     q.count_of_first_pic = Answers.objects.filter(question=q,answer='1').count()
        #     q.count_of_second_pic = Answers.objects.filter(question=q,answer='2').count()
        #
        # context['question_list'] = questions
        # return context


class AdvertiseDetail(DetailView):
    model = Advertises
    template_name = 'advertises/detail.html'

    # @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AdvertiseDetail, self).get_context_data(**kwargs)
        context['object'].price = format(int(context['object'].price), ",d")
        return context
        #
        # def get_context_data(self, **kwargs):
        #     context = super(QuestionsEdit, self).get_context_data(**kwargs)
        #     questions = Questions.objects.filter(test=kwargs['object'])
        #
        #     context['question_list'] = questions
        #     return context


class UpdateAdvertise(UpdateView):
    form_class = UpdateAdvertiseForm
    queryset = Advertises.objects.all()
    template_name = 'advertises/update.html'

    @method_decorator(login_required)
    @method_decorator(mobile_confirmed_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


def create_fake_ads(request):
    user = request.user
    fake = Faker()
    for i in range(int(request.GET['num'])):
        company_name = fake.name()
        description = 'same'
        type = 1
        register_year = 1392
        register_location = 2
        financial_code = 1
        price = 20000
        ad = Advertises.objects.create(user=user, company_name=company_name, description=description, type=type,
                                       register_year=register_year, register_location=register_location,
                                       financial_code=financial_code, price=price)

    return HttpResponse('ok')


def ajax_list(request):
    # numbers_list = range(1, 1000)
    # page = request.GET.get('page', 1)
    # paginator = Paginator(numbers_list, 20)
    # try:
    #     numbers = paginator.page(page)
    # except PageNotAnInteger:
    #     numbers = paginator.page(1)
    # except EmptyPage:
    #     numbers = paginator.page(paginator.num_pages)
    return render(request, 'advertises/list_page.html')
