from django.apps import AppConfig


class AdvertisesConfig(AppConfig):
    name = 'advertises'

    def ready(self):
        import advertises.signals
