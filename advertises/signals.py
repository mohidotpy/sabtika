from django.db.models.signals import post_save, post_delete
from company_advertise_invoice.models import CompanyLadderAdvertiseInvoice, CompanyUrgentAdvertiseInvoice
from django.utils import timezone


def update_ladder_date_when_payment_done(sender, instance, **kwargs):
    if not kwargs['created']:
        if 'payment' in kwargs['update_fields']:
            if instance.payment == True:
                advertise = instance.company_advertise
                advertise.ladder_date = timezone.now()
                advertise.save()


post_save.connect(update_ladder_date_when_payment_done, sender=CompanyLadderAdvertiseInvoice)


def update_urgent_when_payment_done(sender, instance, **kwargs):
    if not kwargs['created']:
        if instance.payment == True:
            advertise = instance.company_advertise
            advertise.urgent = True
            advertise.save()


post_save.connect(update_urgent_when_payment_done, sender=CompanyUrgentAdvertiseInvoice)
