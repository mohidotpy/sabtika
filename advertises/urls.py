from django.urls import path
from . import views

app_name = 'advertises'

urlpatterns = [
    path('list/', views.AdvertisesList.as_view(), name='list'),
    path('mine/', views.MyAdvertisesList.as_view(), name='my_list'),
    path('<int:pk>/', views.AdvertiseDetail.as_view(), name='detail'),
    path('create/', views.CreateAd.as_view(), name='create'),
    path('statistic/<int:pk>/', views.Statistic.as_view(), name='statistic'),
    path('update/<int:pk>/', views.UpdateAdvertise.as_view(), name='update'),
    # path('<int:pk>/questions_edit/', views.QuestionsEdit.as_view(), name='questions_edit'),
    path('fake/', views.create_fake_ads, name='fake'),

]
