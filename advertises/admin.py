from django.contrib import admin
from .models import Advertises


class WithdrawalAdmin(admin.ModelAdmin):
    list_display = ('user',)


admin.site.register(Advertises, WithdrawalAdmin)
