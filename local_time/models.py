from django.db import models
import datetime
import pytz


# Create your models here.
class LocalTime():
    @staticmethod
    def convert_today_utc_to_iran_utc():
        today = datetime.datetime.utcnow().date()
        start_today_utc = datetime.datetime.combine(today, datetime.time(0, 0, 0))
        start_today_iran = start_today_utc - datetime.timedelta(hours=3, minutes=30)
        end_today_tehran = start_today_iran + datetime.timedelta(hours=23, minutes=59, seconds=59, microseconds=999999)
        return start_today_iran, end_today_tehran

    @staticmethod
    def convert_iran_today_to_naive():
        utc = pytz.UTC
        regular_time = LocalTime.convert_today_utc_to_iran_utc()
        naive_start_today_iran = utc.localize(regular_time[0])
        naive_end_today_iran = utc.localize(regular_time[1])
        return naive_start_today_iran, naive_end_today_iran
