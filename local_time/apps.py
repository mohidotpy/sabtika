from django.apps import AppConfig


class LocalTimeConfig(AppConfig):
    name = 'local_time'
