from django.core import exceptions
from django.utils.translation import gettext as _

HTTP_498_REVOKED = 498
HTTP_419_DID_NOT_SEND = 419


# TODO wrap default detail to add to a dict with "detail" key.









class MaxQuestionNumberReached(exceptions.APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _("you already have 2 cashtags. you can't have more. if it's necessary, contact us.")


class ClientIdIsRequired(exceptions.APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _("Client id is required.")


class NotEnoughBalance(exceptions.APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _("not enough balance.")
