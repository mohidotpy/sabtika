from functools import wraps
from django.shortcuts import redirect
from django.contrib import messages
from django.utils.translation import gettext as _


def email_confirmed_required(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        if request.user.is_verified:
            return function(request, *args, **kwargs)
        messages.add_message(request, messages.ERROR, _('You have to confirm your email first.'))
        return redirect('users:dashboard')

    return wrap


def mobile_confirmed_required(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        if request.user.is_phone_number_verified:
            return function(request, *args, **kwargs)
        messages.add_message(request, messages.ERROR, 'شما باید ابتدا شماره تلفن خود را تایید کنید.')
        return redirect('users:confirm_phone_number')

    return wrap


def mobile_not_confirmed_required(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        if not request.user.is_phone_number_verified:
            return function(request, *args, **kwargs)
        messages.add_message(request, messages.ERROR, 'شماره موبایل شما تایید شده است.')
        return redirect('users:dashboard')

    return wrap


def bank_info_required(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        if request.user.account_number and request.user.shaba_number and request.user.card_number:
            return function(request, *args, **kwargs)
        messages.add_message(request, messages.ERROR, _('You have to provide bank numbers'))
        return redirect('users:dashboard')

    return wrap


def admin_required(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        if request.user.is_admin:
            return function(request, *args, **kwargs)
        messages.add_message(request, messages.ERROR, _('access is restricted.'))
        return redirect('users:dashboard')

    return wrap
