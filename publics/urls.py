from django.urls import path
from . import views

app_name = 'publics'

urlpatterns = [
    path('', views.root, name='root'),
    path('about_us/', views.about_us, name='about_us'),
    path('privacy_policy/', views.privacy_policy, name='privacy_policy'),
    path('support/', views.support, name='support'),
]
