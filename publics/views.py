from django.shortcuts import render, redirect
from django.http import HttpResponse

def root(request):
    return render(request, template_name='publics/root.html')

def about_us(request):
    return render(request, template_name='publics/about_us.html')

def privacy_policy(request):
    return render(request, template_name='publics/privacy_policy.html')

def support(request):
    return render(request, template_name='publics/support.html')