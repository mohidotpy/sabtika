number_to_persian_letter = {'1': 'اول',
                            '2': 'دوم',
                            '3': 'سوم',
                            '4': 'چهارم',
                            '5': 'پنجم',
                            '6': 'ششم',
                            '7': 'هفتم',
                            '8': 'هشتم',
                            '9': 'نهم',
                            '10': 'دهم', }

number_test_to_block = 20

max_image_test_size = 1024 * 1024 * 1
max_profile_size = 1024 * 1024 * 0.5

acceptable_image_type = ['image/jpeg', 'image/jpg', 'image/png']

per_transaction_wage = 100

profile_picture_size = (512, 512)
