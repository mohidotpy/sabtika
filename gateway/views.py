from django.shortcuts import render
from django.http import HttpResponse
from zeep import Client
from .constants import MERCHANT, gateway_secret
from django.views.decorators.http import require_POST
from .models import Gateway
from invoice.models import Invoice
from django.shortcuts import redirect
from django.urls import reverse
import os
from company_advertise_invoice.models import CompanyAdvertiseInvoice, CompanyLadderAdvertiseInvoice, \
    CompanyUrgentAdvertiseInvoice
from engineer_advertise_invoice.models import EngineerAdvertiseInvoice, EngineerLadderAdvertiseInvoice, \
    EngineerUrgentAdvertiseInvoice
from engineer_advertise_invoice.models import EngineerAdvertiseInvoice
from django.http import Http404
from django.core.validators import ValidationError
from django.utils.translation import gettext_lazy as _

# Create your views here.



client = Client('https://www.zarinpal.com/pg/services/WebGate/wsdl')
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# BaseCallbackURL = [
#     os.path.join(BASE_DIR, "verify"),
#     # '/var/www/static/',
# ]


@require_POST
def send_request(request):
    amount = request.POST['amount']

    invoice_id = str(request.POST['invoice_id'])
    class_name = str(request.POST['class_name'])
    invoice_type = str(request.POST['type'])
    encoded_string = Gateway.encode(gateway_secret, invoice_id)
    kwarg = {'class_name': class_name,
             'encoded_string': encoded_string,
             'invoice_type': invoice_type}

    callback_url = request.scheme + '://' + request.META['HTTP_HOST'] + reverse('gateway:verify',
                                                                                kwargs=kwarg)

    if amount == '0':
        return redirect(callback_url + '?Status=zero')

    description = 'پرداخت مربوط به ' + request.POST['email']
    email = request.POST['email']
    mobile = None

    result = client.service.PaymentRequest(MERCHANT, amount, description, email, mobile, callback_url)
    if result.Status == 100:
        return redirect('https://www.zarinpal.com/pg/StartPay/' + str(result.Authority))
    else:
        return HttpResponse('Error code: ' + str(result.Status))


def verify(request, encoded_string, class_name, invoice_type):
    try:
        if request.GET.get('Status') == 'OK':
            invoice_id = Gateway.decode(gateway_secret, encoded_string)

            if class_name == 'ca_invoice':
                if invoice_type == 'regular':
                    target_class = CompanyAdvertiseInvoice
                elif invoice_type == 'ladder':
                    target_class = CompanyLadderAdvertiseInvoice
                elif invoice_type == 'urgent':
                    target_class = CompanyUrgentAdvertiseInvoice
                else:
                    raise ValidationError({'detail': _('not valid invoice type')})



            elif class_name == 'en_invoice':
                if invoice_type == 'regular':
                    target_class = EngineerAdvertiseInvoice
                elif invoice_type == 'ladder':
                    target_class = EngineerLadderAdvertiseInvoice
                elif invoice_type == 'urgent':
                    target_class = EngineerUrgentAdvertiseInvoice
                else:
                    raise ValidationError({'detail': _('not valid invoice type')})
            else:
                raise ValidationError({'detail': _('not valid calss')})

            invoice = target_class.objects.get(pk=invoice_id)

            result = client.service.PaymentVerification(MERCHANT, request.GET['Authority'], int(invoice.amount))
            if result.Status == 100:
                invoice.payment = 1
                invoice.tracking_code = result.RefID
                invoice.save(update_fields=['payment', 'tracking_code'])
                return redirect(class_name + ':history')
            elif result.Status == 101:
                return HttpResponse('Transaction submitted : ' + str(result.Status))
            else:
                return HttpResponse('Transaction failed.\nStatus: ' + str(result.Status))
        elif request.GET.get('Status') == 'zero':
            invoice_id = Gateway.decode(gateway_secret, encoded_string)

            if class_name == 'ca_invoice':
                if invoice_type == 'regular':
                    target_class = CompanyAdvertiseInvoice
                elif invoice_type == 'ladder':
                    target_class = CompanyLadderAdvertiseInvoice
                elif invoice_type == 'urgent':
                    target_class = CompanyUrgentAdvertiseInvoice
                else:
                    raise ValidationError({'detail': _('not valid invoice type')})



            elif class_name == 'en_invoice':
                if invoice_type == 'regular':
                    target_class = EngineerAdvertiseInvoice
                elif invoice_type == 'ladder':
                    target_class = EngineerLadderAdvertiseInvoice
                elif invoice_type == 'urgent':
                    target_class = EngineerUrgentAdvertiseInvoice
                else:
                    raise ValidationError({'detail': _('not valid invoice type')})
            else:
                raise ValidationError({'detail': _('not valid calss')})

            invoice = target_class.objects.get(pk=invoice_id)

            invoice.payment = 1
            invoice.tracking_code = 0
            invoice.save(update_fields=['payment', 'tracking_code'])
            return redirect(class_name + ':history')
        else:
            return HttpResponse('Transaction failed or canceled by user')
    except CompanyAdvertiseInvoice.DoesNotExist:
        raise Http404
    except EngineerAdvertiseInvoice.DoesNotExist:
        raise Http404
    except ValidationError:
        raise Http404
