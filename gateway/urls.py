from django.urls import path
from . import views

app_name = 'gateway'
urlpatterns = [
    path('request/', views.send_request, name='request'),
    path('verify/<str:class_name>/<str:invoice_type>/<str:encoded_string>/', views.verify, name='verify'),
]
