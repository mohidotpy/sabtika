from django.forms import ModelForm
from .models import Users
from django import forms
from django.utils.translation import gettext as _
from PIL import Image
from io import BytesIO
from django.core.files import File
from publics.constants import profile_picture_size, max_profile_size, acceptable_image_type


class SignUpUsersForm(ModelForm):
    privacy = forms.BooleanField()

    def __init__(self, *args, **kwargs):
        super(SignUpUsersForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs = {'class': 'validate'}
        self.fields['password'].widget.attrs = {'class': 'validate', 'pattern': '^[a-zA-Z0-9]{4,24}$'}
        self.fields['phone_number'].widget.attrs = {'class': 'validate', 'pattern': '^09\d{9}$',
                                                    'title': 'شماره موبایل 11 رقم بوده و با 09 شروع میشود.'}

    def is_valid(self):
        for field in self.fields:
            if field in self.errors:
                self.fields[field].widget.attrs['class'] += ' invalid'

                for index, error in enumerate(self.errors[field]):
                    self.errors[field][index] = _(error)

        return super(SignUpUsersForm, self).is_valid()

    class Meta:
        model = Users
        fields = ['email', 'password', 'phone_number']
        widgets = {
            'password': forms.PasswordInput(),
            'email': forms.EmailInput(),
        }

    # set_password is for hashing password before call save in model . if set True. if false , nothing happend. also set_password value not passed to super
    def save(self, commit=True, set_password=True):
        if set_password:
            self.instance.set_password(self.instance.password)
        user = super(SignUpUsersForm, self).save(commit=commit)
        return user


class LoginUsersForm(forms.Form):
    email = forms.EmailField(required=True)
    password = forms.CharField(max_length=24, widget=forms.PasswordInput)


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Users
        fields = ['first_name', 'profile_picture', 'phone_number', 'instagram', 'telegram']

    def clean(self):
        if self.cleaned_data:
            if 'profile_picture' in self.cleaned_data:
                if self.cleaned_data['profile_picture'] != None:
                    # if first error appear, django remove first pic from clean data
                    size_error, postfix_error = False, False

                    if self.cleaned_data['profile_picture'].size > max_profile_size:
                        size_error = True
                    if 'content_type' in dir(self.cleaned_data['profile_picture']):
                        if self.cleaned_data['profile_picture'].content_type not in acceptable_image_type:
                            postfix_error = True

                    if postfix_error:
                        self.add_error('profile_picture', _("the only acceptable files format are jpeg and png"))
                    if size_error:
                        self.add_error('profile_picture', _("profile picture have to be less than 1 megabyte"))

                    if self.errors != {}:
                        return super(ProfileForm, self).clean()

                    if 'content_type' in dir(self.cleaned_data['profile_picture']):
                        size = profile_picture_size
                        profile_picture = self.cleaned_data['profile_picture']
                        im = Image.open(profile_picture)
                        im.thumbnail(size, Image.ANTIALIAS)
                        out = BytesIO()
                        im.save(out, format=profile_picture.image.format)
                        self.cleaned_data['profile_picture'] = File(out, name=profile_picture.name)
        return super(ProfileForm, self).clean()



        # def clean_profile_picture(self):
        #     size = (512, 512)
        #     profile_picture = self
        #     im = Image.open(profile_picture)
        #     im.thumbnail(size, Image.ANTIALIAS)
        #     out = BytesIO()
        #     im.save(out, format='jpeg', quality=99)


class UpdateBankForm(forms.ModelForm):
    class Meta:
        model = Users
        fields = ['account_number', 'shaba_number', 'card_number']
