from django.urls import path
from . import views
app_name = 'users'
urlpatterns = [
    path('signup/', views.SignupUsersView.as_view(), name='signup'),
    path('login/', views.LoginUser, name='login'),
    path('logout/', views.Logout, name='logout'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('profile/', views.ProfileUsersView.as_view(), name='profile'),
    path('profile/confirm_phone_number', views.confirm_phone_number, name='confirm_phone_number'),
    path('bank_info/', views.UpdateBankInfo, name='bank_info'),
    path('forgot_password/', views.MyPasswordRestartView.as_view(), name='forgot_password'),
    path('password_reset_confirm/<uidb64>/<token>/', views.MyPasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('password_reset_done/', views.MyPasswordResetDoneView.as_view(),
         name='password_reset_done'),
    path('password_reset_complete/', views.MyPasswordResetCompleteView.as_view(),
             name='password_reset_complete'),


]
