import re


def melli_code_validator(melli_code):
    regex = re.compile("[0-9]{10}")
    if regex.match(melli_code):
        return True
    return False


def pk_validator(pk):
    regex = re.compile("^[0-9]*$")
    if regex.match(pk):
        return True
    return False
