from django.db import models
from django.core.validators import EmailValidator, MinLengthValidator
from .validators import melli_code_validator
from django.utils.translation import ugettext_lazy as _
import re
from django.contrib.auth.base_user import AbstractBaseUser
from .managers import UserManager
from django.core.validators import ValidationError, RegexValidator
from PIL import Image
from io import BytesIO
from django.core.files import File
from publics.constants import profile_picture_size

import secrets


# Create your models here.


class Users(AbstractBaseUser):
    TESTER = 1
    TEST_OWNER = 2

    USER_ROLE_CHOICES = (
        (TESTER, _('Tester')),
        (TEST_OWNER, _('TestOwner'))
    )

    FEMALE, MALE = range(2)

    GENDER_CHOICE = (
        (FEMALE, _('Female')),
        (MALE, _('Male'))
    )

    objects = UserManager()

    USERNAME_FIELD = 'email'
    # REQUIRED_FIELDS = ['phone_number']

    email = models.CharField(_('email'), db_index=True, max_length=50,
                             validators=[EmailValidator()], unique=True, null=False)
    # TODO phone number needed to be confirmed
    phone_number = models.CharField(_('phone number'), db_index=True, max_length=11,
                                    validators=[MinLengthValidator(11)], unique=True, null=True)
    is_phone_number_verified = models.BooleanField(default=False)
    phone_number_verification_code = models.CharField(max_length=128,
                                                      validators=[RegexValidator(regex='^[a-zA-Z0-9]{4,24}$')],
                                                      null=True)
    password = models.CharField(_('password'), validators=[RegexValidator(regex='^[a-zA-Z0-9]{4,24}$', message=_(
        'Password must be Alphanumeric , minimum 4 and maxium 24 charecter'),
                                                                          code='invalid_password')], max_length=128,
                                null=True,
                                blank=True)
    first_name = models.CharField(_('First Name'), max_length=50, null=True, blank=True)
    last_name = models.CharField(_('Last Name'), max_length=100, null=True, blank=True)
    card_number = models.CharField(_('Card Number'), max_length=16, null=True, blank=True,
                                   validators=[RegexValidator(regex='^[1-9]\d{15}$')])
    account_number = models.CharField(_('Account Number'), max_length=22, null=True, blank=True,
                                      validators=[RegexValidator(regex='^\d*$')])
    shaba_number = models.CharField(_('Shaba Number'), max_length=26, validators=[RegexValidator(regex='^IR[\d]{24}$')],
                                    null=True, blank=True)
    # TODO melli code can change just one time after confirmation
    melli_code = models.CharField(validators=[melli_code_validator], null=True, blank=True, max_length=10)
    profile_picture = models.ImageField(null=True, blank=True, upload_to='user_data/profile_picture/')
    birth_date = models.DateField(null=True, blank=True)
    role = models.PositiveSmallIntegerField(choices=USER_ROLE_CHOICES, default=TESTER)
    gender = models.PositiveSmallIntegerField(choices=GENDER_CHOICE, null=True, blank=True)
    instagram = models.CharField(max_length=100, null=True,blank=True)
    telegram = models.CharField(max_length=100, null=True,blank=True)
    is_pro = models.BooleanField(default=False)
    is_banned = models.BooleanField(_('is banned'), default=False)

    # For django's built-in superusers
    is_admin = models.BooleanField(_('admin status'), default=False)
    # Is active field tells if user is an active user or is a previous user of the system.
    # If user is inactive he/she only can view related data.
    is_active = models.BooleanField(_('active'), default=True)

    # Is verified field tells if user has verified his/her phone number or not.
    is_verified = models.BooleanField(_('verified'), default=False)

    # @property
    # def user_node(self):
    #     if self.is_authenticated:
    #         return self.insure_integrity_of_user_node()
    #     return None
    REQUIRED_FIELDS = []

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return self.email

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    # def save(self, *args, **kwargs):
    #     self.set_password(self.password)
    #     return super(Users, self).save(*args, **kwargs)

    def create(self, validated_data):
        """
        Create and return a new `User` instance, given the validated data.
        """
        if not validated_data['email']:
            raise ValueError('Email is required.')
        if validated_data['role'] not in (1, 2):
            raise ValueError('Role not defined.')
        role = 1
        is_active = False

        user = Users(email=validated_data['email'], is_active=is_active, role=role)
        user.set_password(validated_data['password'])
        user.save()

        return user

    # TODO what?
    def update(self, instance, validated_data):
        """
        Update and return an existing `Snippet` instance, given the validated data.
        """
        instance.melli_code = validated_data.get('title', instance.melli_code)
        instance.profile_picture = validated_data.get('code', instance.profile_picture)
        instance.birthday_date = validated_data.get('linenos', instance.birthday_date)
        instance.role = validated_data.get('language', instance.role)
        instance.mobile_number_varified = validated_data.get('style', instance.mobile_number_varified)
        instance.gender = validated_data.get('style', instance.gender)

        instance.save()
        return instance

    @staticmethod
    def is_phone_number_correct(phone_number):
        phone_number = Users.correct_phone_number_structure(phone_number)
        pattern = re.compile("^09\d{9}$")
        return bool(pattern.match(phone_number))

    @staticmethod
    def correct_phone_number_structure(phone_number):
        pattern_1 = re.compile("^9\d{9}$")
        pattern_2 = re.compile("^09\d{9}$")
        pattern_3 = re.compile("^00989\d{9}$")
        pattern_4 = re.compile("^\+989\d{9}$")

        if bool(pattern_1.match(phone_number)):
            return "0" + phone_number
        if bool(pattern_2.match(phone_number)):
            return phone_number
        if bool(pattern_3.match(phone_number)):
            return "0" + phone_number[4:]
        if bool(pattern_4.match(phone_number)):
            return "0" + phone_number[3:]

        # just for readability
        raise ValueError('phone number is not valid.')

    @staticmethod
    def check_if_phone_number_correct(phone_number):
        # this method use for cases we dont want to check and catch exception directly. for example in views
        if not Users.is_phone_number_correct(phone_number):
            raise ValidationError({'detail': _('your phone number is not correct.enter a correct one.')})

    @staticmethod
    def get_user_by_phone_number(phone_number):
        return Users.objects.filter(phone_number=phone_number).first()

    def get_absolute_url(self):
        return u'/users/login/?register=True'

    @staticmethod
    def create_one_time_pin(digit_count=6):
        start = int("1" + ("0" * (digit_count - 1)))
        stop = int("9" * digit_count)
        return secrets.choice(range(start, stop))

    def check_confirm_code(self, code):
        if self.phone_number_verification_code == code:
            return True
        return False

    def handle_sent_confirmation_code(self, code):
        if self.check_confirm_code(code):
            self.phone_number_verification_code = None
            self.is_phone_number_verified = True
            self.save()
            return
        else:
            raise ValidationError({'detail': 'کد ارسال شده اشتباه است'})

    # def save(self, force_insert=False, force_update=False, using=None,
    #          update_fields=None):
    #     db_wt = Users.objects.filter(pk=self.pk).first()
    #     if db_wt != None:
    #         if db_wt.phone_number != self.phone_number:
    #             update_fields = ['phone_number']
    #         return super(Users, self).save(force_insert=False, force_update=False, using=None,
    #                                        update_fields=update_fields)
    #     return super(Users, self).save(force_insert=False, force_update=False, using=None,
    #                                    update_fields=None)
