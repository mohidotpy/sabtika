from django.contrib import admin
from .models import Users


class WithdrawalAdmin(admin.ModelAdmin):
    list_display = ('email',)


admin.site.register(Users, WithdrawalAdmin)
