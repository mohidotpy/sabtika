from django.views import generic
from .forms import SignUpUsersForm, LoginUsersForm, ProfileForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.template.loader import render_to_string
from emails.tokens import account_activation_token
from django.contrib.auth import login, logout
from django.contrib import messages
from django.utils.translation import gettext as _
from .tasks import send_email
from .models import Users
from django.urls import reverse
from .forms import UpdateBankForm
# from select_test.models import SelectTest
from django.urls import reverse_lazy
from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, \
    PasswordResetCompleteView
from advertises.models import Advertises
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.exceptions import ValidationError
from publics.decorators import mobile_not_confirmed_required
from engineer_advertises.models import EngineerAdvertises


class ProfileUsersView(generic.UpdateView):
    form_class = ProfileForm
    template_name = 'users/profile.html'
    queryset = Users.objects.all()

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, _('Your profile updated successfully '))
        return reverse('users:dashboard')

    def get_object(self, queryset=None):
        return Users.objects.get(id=self.request.user.id)


class SignupUsersView(generic.CreateView):
    form_class = SignUpUsersForm
    template_name = 'users/signup.html'
    context_object_name = 'users'

    def get(self, request, *args, **kwargs):
        if not request.user.is_anonymous:
            return redirect('users:dashboard')
        # TODO next : agar ?register=True ye peighami neshun bede
        return super(SignupUsersView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if not request.user.is_anonymous:
            return redirect('users:dashboard')

        result = super(SignupUsersView, self).post(request, *args, **kwargs)
        if self.object != None:
            login(request, self.object)

        return result

    def form_valid(self, form):
        try:
            if form.cleaned_data['privacy'] != True:
                messages.add_message(self.request, messages.ERROR, _('شما باید قوانین و مقررات را قبول کنید.'))
                return redirect('users:signup')
            self.object = form.save(commit=False, set_password=False)
            self.object.user = self.request.user
            self.object.user.is_varified = False
            self.object.user.is_admin = False
            self.object.phone_number_verification_code = Users.create_one_time_pin()
            self.object.save()
            # send confirmation email to user
            user = self.object
            current_site = get_current_site(self.request)
            subject = 'ایمیل تاییدیه|ثبتیکا'
            message = render_to_string('emails/confirm_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode("utf-8"),
                'token': account_activation_token.make_token(user),
            })
            # reception is a dict even with one email
            # email = EmailMessage(subject, message, from_email='info@kodume.ir', to=[user.email])
            # email.send()

            # TODO celery have to send emails
            send_email.delay(subject=subject, html_message=message, receptions=[user.email])
        except Exception as e:
            print(e)

        return super(SignupUsersView, self).form_valid(form)


# class LoginUsersView(generic.CreateView):
#     form_class = SignUpUsersForm
#     model = Users
#     template_name = 'users/login.html'
#     context_object_name = 'users'
#
#     def get(self, request, *args, **kwargs):
#         # TODO next : agar ?register=True ye peighami neshun bede
#         return super(LoginUsersView, self).get(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         return super(LoginUsersView, self).post(request, *args, **kwargs)


def LoginUser(request):
    if not request.user.is_anonymous:
        return redirect('users:dashboard')

    if request.method == 'GET':
        form = LoginUsersForm()

    elif request.method == 'POST':
        form = LoginUsersForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            user = authenticate(email=email, password=password)
            if user is not None:
                login(request, user)
                return redirect('users:dashboard')
            else:
                messages.add_message(request, messages.ERROR, _('Email or password is wrong'))

    return render(request, 'users/login.html', {'form': form})


@login_required
@mobile_not_confirmed_required
def confirm_phone_number(request):
    user = request.user
    if request.method == 'GET':
        if user.is_phone_number_verified:
            messages.add_message(request, messages.WARNING, ('شماره شما تایید شده است.'))
            return redirect('users:dashboard')
        else:
            return render(request, 'users/confirm_phone_number.html')
    elif request.method == "POST":
        try:
            user.handle_sent_confirmation_code(request.POST['id_confirmation_code'])
            messages.add_message(request, messages.SUCCESS, ('شماره موبایل شما با موفقیت تایید شد.'))
            return redirect('users:dashboard')

        except ValidationError:
            messages.add_message(request, messages.ERROR, ('کد ارسال شده اشتباه است. دوباره تلاش کنید.'))
            return redirect('users:confirm_phone_number')


def Logout(request):
    logout(request)
    return redirect('users:login')


@login_required
def dashboard(request):
    user_ads = Advertises.objects.filter(user=request.user).count()
    user_en_ads = EngineerAdvertises.objects.filter(user=request.user).count()
    return render(request, 'users/dashboard.html', {'user_ads': user_ads,'user_en_ads':user_en_ads})


@login_required()
def UpdateBankInfo(request):
    if request.method == 'POST':
        form = UpdateBankForm(request.POST)
        if form.is_valid():
            user = request.user
            user.account_number = form.cleaned_data.get('account_number')
            user.shaba_number = form.cleaned_data.get('shaba_number')
            user.card_number = form.cleaned_data.get('card_number')
            user.save()
            messages.add_message(request, messages.SUCCESS, _('Your bank info updated successfully.'))
            return redirect('users:dashboard')

    else:
        messages.add_message(request, messages.SUCCESS, _('access directly restricted.'))
        return redirect('users:dashboard')


class MyPasswordRestartView(PasswordResetView):
    template_name = 'users/reset_password.html'
    email_template_name = 'emails/password_reset_email.html'
    from_email = 'sabtika.ir@gmail.com'
    success_url = reverse_lazy('users:password_reset_done')


class MyPasswordResetConfirmView(PasswordResetConfirmView):
    template_name = 'users/password_reset_confirm.html'
    success_url = reverse_lazy('users:password_reset_complete')

    def form_valid(self, form):
        form_valid = super(MyPasswordResetConfirmView, self).form_valid(form)
        if form.is_valid():
            user = self.user
            user.is_verified = True
            user.save()
        return form_valid


class MyPasswordResetDoneView(PasswordResetDoneView):
    template_name = 'users/password_reset_done.html'


class MyPasswordResetCompleteView(PasswordResetCompleteView):
    template_name = 'users/password_reset_complete.html'
