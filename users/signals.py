from django.db.models.signals import post_save, post_delete, pre_save
from .models import Users
from sms.models import Sms
from django.contrib import messages


# def change_to_pro(sender, instance, **kwargs):
#     if instance.upgraded_user != None:
#         user = instance.upgraded_user
#         if instance.payment == 1:
#             user.is_pro = True
#             user.save()
#
#

def check_if_phone_number_changed(sender, instance, **kwargs):
    current_user = Users.objects.filter(pk=instance.pk).first()
    if current_user:
        if current_user.phone_number != instance.phone_number:
            instance.__phone_number_changed = True


pre_save.connect(check_if_phone_number_changed, sender=Users)


def set_conf_code_andsend_sms_after_update(sender, instance, **kwargs):
    if hasattr(instance,'__phone_number_changed'):
        if instance.__phone_number_changed:
            code = Users.create_one_time_pin()
            instance.phone_number_verification_code = code
            instance.is_phone_number_verified = False
            Sms.send_varification_code(instance.phone_number, code)
            del instance.__phone_number_changed
            instance.save()


post_save.connect(set_conf_code_andsend_sms_after_update, sender=Users)
