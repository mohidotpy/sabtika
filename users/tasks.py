from __future__ import absolute_import
from celery import shared_task
from django.core.mail import EmailMultiAlternatives
from django.core.mail import get_connection
from emails.constants import pri_host, pri_password, pri_port, pri_use_tls, pri_username


@shared_task  # Use this decorator to make this a asyncronous function
def send_email(subject, html_message, receptions):
    # reception is a dict even with one email
    connection = get_connection(host=pri_host,
                                port=pri_port,
                                username=pri_username,
                                password=pri_password,
                                use_tls=pri_use_tls)

    email = EmailMultiAlternatives(subject, 'this is important', from_email='info@sabtika.ir', to=receptions,
                                   connection=connection)
    email.attach_alternative(html_message, "text/html")

    email.send()

    return True
