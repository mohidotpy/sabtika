from django.urls import path
from . import views

app_name = 'sms'
urlpatterns = [
    path('resend/', views.resend_confirm_code, name='resend')
]
