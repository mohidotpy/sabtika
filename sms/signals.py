from django.db.models.signals import post_save, post_delete
from users.models import Users
from .models import Sms


def send_verification_code(sender, instance, **kwargs):
    if kwargs['created'] == True:
        phone_number = instance.phone_number
        varification_code = instance.phone_number_verification_code
        Sms.send_varification_code(phone_number, varification_code)


post_save.connect(send_verification_code, sender=Users)
