from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import Sms
from users.models import Users
from django.shortcuts import redirect
from django.contrib import messages
from django.views.decorators.http import require_GET


# Create your views here.

@login_required
@require_GET
def resend_confirm_code(request):
    user = request.user
    code = Users.create_one_time_pin()
    user.phone_number_verification_code = code
    user.save()
    Sms.send_varification_code(request.user.phone_number, code)
    messages.add_message(request, messages.SUCCESS, ('کد تایید، دوباره ارسال شد.'))
    return redirect('users:confirm_phone_number')
