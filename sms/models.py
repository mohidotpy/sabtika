from django.db import models
import requests
import json
from sabtika.settings import sms_api_key, sms_secret_key, sms_line_number
from django.http import HttpResponseServerError
from django.core.exceptions import ValidationError


# Create your models here.


class Sms():
    def __init__(self):
        self.sms_api_key = sms_api_key
        self.sms_secret_key = sms_secret_key
        self.sms_line_number = sms_line_number

    @staticmethod
    def get_token(api_key, secret_key):
        data = {'UserApiKey': api_key, 'SecretKey': secret_key}
        headers = {'content-type': 'application/json'}
        url = 'http://RestfulSms.com/api/Token'
        req = requests.post(url=url, data=json.dumps(data), headers=headers)
        req = json.loads(req.text)
        return req['TokenKey']

        # def send_sms(self, text, phonenumber):
        #     url = 'http://RestfulSms.com/api/MessageSend'
        #     token = self.get_token(self.sms_api_key, self.sms_secret_key)
        #     headers = {'content-type': 'application/json', 'x-sms-ir-secure-token': token}
        #     data = {"Messages": [text], "MobileNumbers": [phonenumber], "LineNumber": self.sms_line_number,
        #             "SendDateTime": "", "CanContinueInCaseOfError": True}
        #     req = requests.post(url=url, data=json.dumps(data), headers=headers)
        #     req = json.loads(req.text)
        #     if req['IsSuccessful']:
        #         return req
        #     elif not req['IsSuccessful']:
        #         raise SmsDidNotSend
        #     elif req['status'] == 301:
        #         raise SmsDidNotSend

        # TODO change req to res

    @staticmethod
    def send_varification_code(phone_number, code):
        url = "https://RestfulSms.com/api/UltraFastSend"
        token = Sms.get_token(sms_api_key, sms_secret_key)
        headers = {'content-type': 'application/json', 'x-sms-ir-secure-token': token}
        data = {"ParameterArray": [{"Parameter": "VerificationCode", "ParameterValue": code}], "Mobile": phone_number,
                'TemplateId': '9246'}
        req = requests.post(url=url, data=json.dumps(data), headers=headers)
        req = json.loads(req.text)
        if req['IsSuccessful']:
            return req
        elif not req['IsSuccessful']:
            raise ValidationError(message='there is an error, try again later.')
        elif req['status'] == 301:
            raise ValidationError(message='there is an error, try again later.')