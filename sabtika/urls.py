"""sabtika URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', include('publics.urls', namespace='publics')),
    path('admin/', admin.site.urls),
    path('users/', include('users.urls', namespace='users')),
    path('emails/', include('emails.urls', namespace='emails')),
    path('advertises/', include('advertises.urls', namespace='advertises')),
    path('engineer/', include('engineer_advertises.urls', namespace='engineer_advertises')),
    path('withdrawal/', include('withdrawal.urls', namespace='withdrawal')),
    path('ca_invoice/', include('company_advertise_invoice.urls', namespace='ca_invoice')),
    path('en_invoice/', include('engineer_advertise_invoice.urls', namespace='en_invoice')),
    path('sms/', include('sms.urls', namespace='sms')),
    path('gateway/', include('gateway.urls', namespace='gateway')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
