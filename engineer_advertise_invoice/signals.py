from .models import EngineerAdvertiseInvoice
from django.db.models.signals import post_save, post_delete
from engineer_advertises.models import EngineerAdvertises
from .constants import DEFAULT_AMOUNT


def create_en_invoice(sender, instance, **kwargs):
    if kwargs['created'] == True:
        ca_invoice = EngineerAdvertiseInvoice(engineer_advertise=instance, amount=DEFAULT_AMOUNT)
        ca_invoice.save()


post_save.connect(create_en_invoice, sender=EngineerAdvertises)

#
# def update_invoice(sender, instance, **kwargs):
#     if kwargs['update_fields'] != None:
#         invoice = instance.invoice
#         if 'questions_number' in kwargs['update_fields'] or 'users_number' in kwargs['update_fields']:
#             invoice.update_invoice()
#
# post_save.connect(update_invoice, sender=UserTests)
