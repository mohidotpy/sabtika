from django.forms import ModelForm
from .models import EngineerAdvertiseInvoice, EngineerLadderAdvertiseInvoice, EngineerUrgentAdvertiseInvoice


class CreateENInvoiceForm(ModelForm):
    class Meta:
        model = EngineerAdvertiseInvoice
        fields = ['engineer_advertise']


class CreateENLadderInvoiceForm(ModelForm):
    class Meta:
        model = EngineerLadderAdvertiseInvoice
        fields = ['engineer_advertise']


class CreateENUrgentInvoiceForm(ModelForm):
    class Meta:
        model = EngineerUrgentAdvertiseInvoice
        fields = ['engineer_advertise']
