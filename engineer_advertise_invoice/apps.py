from django.apps import AppConfig


class EngineerAdvertiseInvoiceConfig(AppConfig):
    name = 'engineer_advertise_invoice'


    def ready(self):
        import engineer_advertise_invoice.signals
