# Generated by Django 2.1.5 on 2020-06-07 08:38

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('engineer_advertises', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='EngineerAdvertiseInvoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('amount', models.DecimalField(decimal_places=0, max_digits=9)),
                ('payment', models.BooleanField(default=False)),
                ('tracking_code', models.BigIntegerField(null=True)),
                ('unique_code', models.UUIDField(default=uuid.uuid4)),
                ('engineer_advertise', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='engineer_advertises.EngineerAdvertises')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='EngineerLadderAdvertiseInvoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('amount', models.DecimalField(decimal_places=0, max_digits=9)),
                ('payment', models.BooleanField(default=False)),
                ('tracking_code', models.BigIntegerField(null=True)),
                ('unique_code', models.UUIDField(default=uuid.uuid4)),
                ('engineer_advertise', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='engineer_advertises.EngineerAdvertises')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='EngineerUrgentAdvertiseInvoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('amount', models.DecimalField(decimal_places=0, max_digits=9)),
                ('payment', models.BooleanField(default=False)),
                ('tracking_code', models.BigIntegerField(null=True)),
                ('unique_code', models.UUIDField(default=uuid.uuid4)),
                ('engineer_advertise', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='engineer_advertises.EngineerAdvertises')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
