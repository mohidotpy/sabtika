from django.db import models
from invoice.models import Invoice
from engineer_advertises.models import EngineerAdvertises


# Create your models here.
class EngineerAdvertiseInvoice(Invoice):
    engineer_advertise = models.OneToOneField(EngineerAdvertises, on_delete=models.CASCADE)


class EngineerLadderAdvertiseInvoice(Invoice):
    engineer_advertise = models.ForeignKey(EngineerAdvertises, on_delete=models.CASCADE)


class EngineerUrgentAdvertiseInvoice(Invoice):
    engineer_advertise = models.OneToOneField(EngineerAdvertises, on_delete=models.CASCADE)
