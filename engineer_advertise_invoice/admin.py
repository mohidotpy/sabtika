from django.contrib import admin
from .models import EngineerAdvertiseInvoice


class EAInvoice(admin.ModelAdmin):
    list_display = ('user',)

    def user(self, obj):
        return obj.engineer_advertise.user


admin.site.register(EngineerAdvertiseInvoice, EAInvoice)
