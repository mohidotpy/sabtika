from django.views.generic import DetailView, ListView, CreateView
from .models import EngineerAdvertiseInvoice, EngineerLadderAdvertiseInvoice, EngineerUrgentAdvertiseInvoice
from django.contrib import messages
from django.utils.translation import gettext as _
from django.shortcuts import redirect
from .forms import CreateENInvoiceForm, CreateENLadderInvoiceForm, CreateENUrgentInvoiceForm
from django.http import Http404
from django.db.models import Q
from django.db import IntegrityError
from .constants import DEFAULT_AMOUNT
from .constants import INVOICE_TYPES, DEFAULT_AMOUNT, URGENT_AMOUNT, LADDER_AMOUNT


# Create your views here.
def CreateENInvoice(request):
    try:
        if request.method == 'POST':
            if 'type' in request.POST:
                invoice_type = request.POST['type']
            else:
                invoice_type = INVOICE_TYPES[0][0]

            if invoice_type == INVOICE_TYPES[0][0]:
                form = CreateENInvoiceForm(request.POST)
            elif invoice_type == INVOICE_TYPES[1][0]:
                form = CreateENLadderInvoiceForm(request.POST)
            elif invoice_type == INVOICE_TYPES[2][0]:
                form = CreateENUrgentInvoiceForm(request.POST)
            else:
                raise ValueError({'detail': 'invoice type is not valid'})

            amount = DEFAULT_AMOUNT
            if invoice_type == INVOICE_TYPES[1][0]:
                amount = LADDER_AMOUNT
            elif invoice_type == INVOICE_TYPES[2][0]:
                amount = URGENT_AMOUNT

            if form.is_valid():
                invoice = form.save(commit=False)
                invoice.user = request.user
                invoice.amount = amount
                invoice.save()
                return redirect('en_invoice:get', type=invoice_type, pk=invoice.id)
            else:
                for detail, error in form.errors.items():
                    messages.add_message(request, messages.ERROR, error)
                return redirect('advertises:list')

    except IntegrityError:
        upgrade_invoice = EngineerAdvertiseInvoice.objects.get(upgraded_user=request.user)
        messages.add_message(request, messages.WARNING, _('you have an invoice for this ad before.'))
        return redirect('invoice:get', pk=upgrade_invoice.id)
    except ValueError:
        raise Http404


class GetInvoice(DetailView):
    model = EngineerAdvertiseInvoice
    template_name = 'engineer_advertise_invoice/detail.html'
    context_object_name = 'invoice'
    queryset = EngineerAdvertiseInvoice.objects.all()

    def get_queryset(self):
        if self.kwargs['type'] == INVOICE_TYPES[0][0]:
            return EngineerAdvertiseInvoice.objects.all()
        elif self.kwargs['type'] == INVOICE_TYPES[1][0]:
            return EngineerLadderAdvertiseInvoice.objects.all()
        elif self.kwargs['type'] == INVOICE_TYPES[2][0]:
            return EngineerUrgentAdvertiseInvoice.objects.all()
        else:
            raise Http404

    def get(self, request, *args, **kwargs):
        if 'payment' in request.GET:
            invoice = self.get_object()
            invoice.payment = True
            invoice.tracking_code = 456478
            invoice.save()
            messages.add_message(request, messages.SUCCESS, _('your payment successfully done!'))
            return redirect('invoice:history')

        return super(GetInvoice, self).get(request, *args, **kwargs)

    def get_object(self, queryset=None):
        object = super(GetInvoice, self).get_object(queryset=None)
        if object.engineer_advertise.user == self.request.user:
            return object
        else:
            raise Http404


class InvoiceHistory(ListView):
    model = EngineerAdvertiseInvoice
    template_name = 'engineer_advertise_invoice/history.html'

    def get_queryset(self):
        query = '''SELECT * from engineer_advertises_engineeradvertises AS t1
JOIN
(SELECT *,'regular' as type FROM engineer_advertise_invoice_engineeradvertiseinvoice
UNION
SELECT *,'ladder' as type FROM engineer_advertise_invoice_engineerladderadvertiseinvoice
UNION
    SELECT *,'urgent' as type FROM engineer_advertise_invoice_engineerurgentadvertiseinvoice
ORDER BY creation_date) AS t2
on t1.id = t2.engineer_advertise_id
WHERE  t1.user_id=''' + str(self.request.user.id)
        return EngineerAdvertiseInvoice.objects.raw(query)
